FROM node:alpine

#COPY ./certs/* /etc/ssl/certs/
#ENV SSL_CERT_DIR=/etc/ssl/certs
#RUN apk add --update --no-cache openssl ca-certificates && \
#    update-ca-certificates

WORKDIR '/app'

COPY package*.json ./

RUN npm install

COPY ./ ./

RUN npm run build

FROM nginx
EXPOSE 80
COPY --from=0 /app/build /usr/share/nginx/html
